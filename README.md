#### Martin Hassman

<!--
**met/met** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->

- 🏢 **currently work** at [Golemio Prague Data Platform](https://golemio.cz/).
- 🎓 **studied** at [University of Chemistry and Technology in Prague](https://www.vscht.cz/?jazyk=en "Faculty of Food and Biochemical Technology, Department of Biochemistry and Microbiology").
- 🥼 **participated** in: [CZilla](http://www.czilla.cz/ "Czech large community for Mozilla Suite and Mozilla Firefox projects, Post-mortem."),
[Zdroják](https://www.zdrojak.cz/ "Czech magazine about Web Technologies"), [Česko.Digital](https://cesko.digital/en/ "Czech non-profit organization for focused on improving life in the Czech Republic."), [Dámeroušky](https://www.damerousky.cz/en "Czech project for making and distribution of homemade anti-COVID masks"), [Covid Portál](https://covid.gov.cz/en/ "Czech website made by Government and volunteers about COVID-19"), [Movapp](https://www.movapp.cz/ "Open source project helping Ukraininan refugees and local people to communicate to each other and learn languages.").
- ⚙️ **can work as**: product manager, product owner, scrum master, tech writer, copywriter, web developer, team leader, community manager.
- 📣 **organized:** WebExpo, aDevCamp, mDevCamp, Devel.cz.
- 🌍 **love languages**: I speak fluently Czech, English and Spanish, I’m learning Ukrainian, German and Vietnamese and I’m also interested in Polish, French, Malay and some more.
- 🔭 **projects** have mostly at [my GitHub](https://github.com/met)
- 🖧 **social profiles:** [LinkedIn](https://www.linkedin.com/in/hassman/), [Twitter](https://twitter.com/hassmanm), [Facebook](https://www.facebook.com/martin.hassman/), [Instagram](https://www.instagram.com/martinhassman/), [Mastodon](https://masto.ai/@met), [Bluesky](https://bsky.app/profile/hassman.bsky.social), [GoodReads](https://www.goodreads.com/user/show/27218411-martin-hassman).

<details>
  <summary>📜 Some personal information...</summary>

- 📘 **My favorite writer** is Neil Gaiman
- ⚡ **Fun facts:**
  - I have never attended any hackathon as developer, but I have organized three.
  - I founded magazine Zdroják by mistake.
  - During my studies I wrote two fairy tales about biochemistry, that got published in the Czech Bulletin of Biochemistry and were printed in several research institutes in the Czech Republic. During my university exams, some examiners asked me suspiciously: _Are you the guy who wrote that story about Cinderella enzyme?_
  - I have also written two scripts for theatre plays. Both had a chemical topic. One of them was rehearsed and played by children from a basic school, that did not know chemistry at all.
- ✍️ **I like** meeting with people contributing to the World Wide Web and I love doing interviews with them. My favorites:
  - [Håkon Wium Lie: CSS was created to save HTML](https://www.root.cz/texty/hakon-wium-lie-css-was-created-to-save-html/)
  - [Molly E. Holzschlag: Evangelist and Educator](https://www.zdrojak.cz/clanky/molly-e-holzschlag-evangelist-and-educator/)
  - [David Storey: I believe in web standards](https://www.zdrojak.cz/clanky/david-storey-i-believe-in-web-standards/)
- 🎤 **Some of my public talks**:
  - [Movapp.cz - volunteers united](https://www.youtube.com/watch?v=ThY0ZiWmBV8&t=2353s) (short introducion of Movapp project)
  - [The path to better learning](https://www.youtube.com/watch?v=Iznpfe5KPOc) (45min talk at Barcamp Plzen 2019)
  - [How to create addons for World of Warcraft](https://slideslive.com/38921022/jak-se-tvori-addony-pro-world-of-warcraft) (5min lightning talk at Devel 2019 conference)

</details>

<details>
  <summary>📜 Gitlab details...</summary>

![Martin Hassman's gitlab stats](https://gitlab-readme-stats-flax.vercel.app/api?username=met1&show_icons=true)
</details>


Meet me at some conference, e.g. [WebExpo 2024](https://webexpo.cz/), [Agile Prague 2024](https://agileprague.com/).